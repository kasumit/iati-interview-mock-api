from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin
import json
import requests

app = Flask(__name__)

cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

resp = requests.get("https://umitapi.iati.com/rest/airports/12345678901234567890")

@app.route("/airports", methods=["GET", "POST"])
@cross_origin()
def airports():
    return resp.text


@app.route("/flight-search", methods=["POST"])
@cross_origin()
def get_flights():
    search_param = request.get_json(silent=True)
    resp = requests.post("https://umitapi.iati.com/rest/flightSearch/12345678901234567890", json=search_param)
    if resp.status_code == requests.codes.ok:
        flights_resp = json.loads(resp.text)
        if 'result' in flights_resp:
            flights_resp = flights_resp['result']['flights']
            flights = []
            for flight in flights_resp:
                if len(flight['fares']) > 1:
                    for fare in flight['fares']:
                        flight['fare'] = fare
                        flights.append(flight)

            return jsonify(flights)

        return resp.text, 400


    return '', resp.status_code





if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')